import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class TimetablePanel extends JPanel implements TablePanel {

    private DatabaseManager databaseManager;
    private JScrollPane contentPanel = null;

    public TimetablePanel(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        setLayout(new BorderLayout());
        setBackground(MainWindow.backgroundColor);
        addTopPanel();
        addBottomPanel();
        showAll();
    }


    public void addTopPanel() {
        JPanel topPanel = new JPanel();
        topPanel.add(new Label("Таблицы"));
        JButton plannedButton = new JButton("Расписание");
        plannedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showAll();
            }
        });
        topPanel.add(plannedButton);
        JButton regularButton = new JButton("Регулярные рейсы");
        regularButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showRegular();
            }
        });
        topPanel.add(regularButton);
        topPanel.add(new Label("Действия"));
        JButton addButton = new JButton("Добавить рейс");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showAddWindow();
            }
        });
        topPanel.add(addButton);
        JButton delayButton = new JButton("Отложить рейс");
        delayButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showDelayWindow();
            }
        });
        topPanel.add(delayButton);
        JButton cancelButton = new JButton("Отменить рейс");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showCancelWindow();
            }
        });
        topPanel.add(cancelButton);
        JButton realizeButton = new JButton("Завершить рейс");
        realizeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showRealizeWindow();
            }
        });
        topPanel.add(realizeButton);
        add(topPanel, BorderLayout.NORTH);
    }

    @Override
    public void addBottomPanel() {
        JPanel bottomPanel = new JPanel();
        bottomPanel.add(new JLabel("Запросы"));
        add(bottomPanel, BorderLayout.SOUTH);
    }

    @Override
    public void showAll() {
        if (contentPanel != null) {
            this.remove(contentPanel);
        }
        String[] header = {"№ рейса", "Дата", "Тип рейса", "Откуда", "Пересадка", "Куда", "Тип самолета"};
        String[] columnNames = {"FLIGHT_ID", "FLIGHT_DATE", "FLIGHT_TYPE_NAME", "START_POINT", "TRANSFER_POINT", "END_POINT", "TYPE_NAME"};
        contentPanel = new JScrollPane(resultToTable(databaseManager.select(
                "select * from plannedflights join timetable using(reg_flight_id) join routes using(route_id) join" +
                        " flighttypes using(FLIGHT_TYPE_ID) join planetypes on plane_type=type_id left join" +
                        " cancelledflights using(flight_id) where cancelled_flight_id is null order by flight_date"),
                header, columnNames));
        add(contentPanel, BorderLayout.CENTER);
        contentPanel.setVisible(true);
        revalidate();
    }

    public void showRegular() {
        if (contentPanel != null) {
            this.remove(contentPanel);
        }
        String[] header = {"ID регулярного рейса", "Тип рейса", "Откуда", "Пересадка", "Куда", "ПН",
                "ВТ", "СР", "ЧТ", "ПТ", "СБ", "ВС", "Тип самолета", "Длительность, ч.", "Цена билета"};
        String[] columnNames = {"REG_FLIGHT_ID", "FLIGHT_TYPE_NAME", "START_POINT", "TRANSFER_POINT", "END_POINT", "MONDAY",
                "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY", "TYPE_NAME", "DURATION", "PRICE"};

        contentPanel = new JScrollPane(resultToTable(databaseManager.select(
                "select REG_FLIGHT_ID,FLIGHT_TYPE_NAME,START_POINT,TRANSFER_POINT, END_POINT,MONDAY,TUESDAY,WEDNESDAY," +
                        "THURSDAY,FRIDAY,SATURDAY,SUNDAY,TYPE_NAME,DURATION,PRICE from timetable  join routes" +
                        " using(route_id) join flighttypes using(FLIGHT_TYPE_ID) join planetypes on plane_type=type_id order by reg_flight_id"),
                header, columnNames));
        add(contentPanel, BorderLayout.CENTER);
        contentPanel.setVisible(true);
        revalidate();
    }

    public void showAddWindow() {
        final JComboBox flightID = new JComboBox(databaseManager.getRegularFlights());
        final JComboBox plane = new JComboBox(databaseManager.getPlanes());
        final JTextField date = new JTextField();
        JButton addButton = new JButton("Добавить");
        String[] labels = {"№ регулярного рейса", "Самолёт", "Дата"};
        JComponent[] components = {flightID, plane, date};
        InsertWindow insertWindow = initInsertWindow("Добавить рейс", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String success;
                if (!date.getText().equals("")) {
                    String[] columns = {"REG_FLIGHT_ID", "PLANE_ID", "FLIGHT_DATE"};
                    String[] values = {(String) flightID.getSelectedItem(),  (String)plane.getSelectedItem(), toDateCurrentTime(date.getText())};
                    success = databaseManager.insert("PLANNEDFLIGHTS", columns, values);
                    insertWindow.resultMessage(success);
                }
            }
        });
    }
    public void showDelayWindow() {
        final JComboBox flightID = new JComboBox(databaseManager.getFlights());
        final JTextField actualTime = new JTextField();
        final JComboBox reason = new JComboBox(databaseManager.getDelayReasons());
        JButton addButton = new JButton("Отложить");
        String[] labels = {"№ рейса", "Время", "Причина"};
        JComponent[] components = {flightID, actualTime, reason};
        InsertWindow insertWindow = initInsertWindow("Отложить рейс", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String success;
                if (!actualTime.getText().equals("")) {
                    String[] columns = {"FLIGHT_ID", "ACTUAL_TIME", "REASON_ID"};
                    String[] values = {(String) flightID.getSelectedItem(), toDateTime(actualTime.getText()), databaseManager.getDelayReasonByName((String) reason.getSelectedItem())};
                    success = databaseManager.insert("DELAYEDFLIGHTS", columns, values);
                    insertWindow.resultMessage(success);
                }
            }
        });
    }

    public void showCancelWindow() {
        final JComboBox flightID = new JComboBox(databaseManager.getFlights());
        JButton addButton = new JButton("Отменить");
        String[] labels = {"№ рейса"};
        JComponent[] components = {flightID};
        InsertWindow insertWindow = initInsertWindow("Отменить рейс", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String success;
                String[] columns = {"FLIGHT_ID"};
                String[] values = {(String) flightID.getSelectedItem()};
                success = databaseManager.insert("CANCELLEDFLIGHTS", columns, values);
                insertWindow.resultMessage(success);
            }
        });
    }

    public void showRealizeWindow() {
        final JComboBox flightID = new JComboBox(databaseManager.getFlights());
        JButton addButton = new JButton("Завершить");
        String[] labels = {"№ рейса"};
        JComponent[] components = {flightID};
        InsertWindow insertWindow = initInsertWindow("Завершить рейс", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String success;
                String[] columns = {"ROUTE_ID", "PLANE_ID", "FLIGHT_TYPE_ID", "FLIGHT_DATE", "DURATION"};
                String[] values = {databaseManager.getRouteIdByFlightId((String) flightID.getSelectedItem()),
                        databaseManager.getPlaneIdByFlightId((String) flightID.getSelectedItem()),
                        databaseManager.getFlightTypeIdByFlightId((String) flightID.getSelectedItem()),
                        toDateTime(databaseManager.getDateByFlightId((String) flightID.getSelectedItem())),
                        databaseManager.getDurationByFlightId((String) flightID.getSelectedItem())};
                success = databaseManager.insert("REALIZEDFLIGHTS", columns, values);
                insertWindow.resultMessage(success);
            }
        });
    }


}
