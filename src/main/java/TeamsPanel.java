import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TeamsPanel extends JPanel implements TablePanel {

    private DatabaseManager databaseManager;

    public TeamsPanel(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        setLayout(new BorderLayout());
        setBackground(MainWindow.backgroundColor);
        addTopPanel();
        addBottomPanel();
        showAll();
    }

    @Override
    public void addTopPanel() {
        JPanel topPanel = new JPanel();
        topPanel.add(new Label("Бригады"));
        JButton addButton = new JButton("Добавить бригаду");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showInsertWindow();
            }
        });
        JButton structureButton = new JButton("Состав бригад");
        structureButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showTeamStructure();
            }
        });
        topPanel.add(addButton);
        topPanel.add(structureButton);
        add(topPanel, BorderLayout.NORTH);
    }

    @Override
    public void addBottomPanel() {
        JPanel bottomPanel = new JPanel();
        bottomPanel.add(new JLabel("Запросы"));
        add(bottomPanel, BorderLayout.SOUTH);
    }

    @Override
    public void showAll() {
        String[] header = {"№ бригады", "Отдел"};
        String[] columnNames = {"TEAM_ID", "DEP_NAME"};
        JScrollPane jScrollPane = new JScrollPane(resultToTable(databaseManager.select(
                "select team_id, dep_name from teams join departments using (dep_id) order by dep_name"),
                header, columnNames));
        add(jScrollPane, BorderLayout.CENTER);
    }

    public ResultSet getTable() {
        return databaseManager.getAllFromTable("TEAMS");
    }

    void showInsertWindow() {
        int ipadX = 100, ipadY = 15, iPadTextX = 300;
        final InsertWindow insertWindow = new InsertWindow("Добавление бригады", 500, 200);

        final JTextField teamID = new JTextField();
        final JComboBox depName = new JComboBox(databaseManager.getDepartments());
        JButton addButton = new JButton("Добавить");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String success;

                if (!teamID.getText().equals("")) {
                    String[] columns = {"TEAM_ID", "DEP_ID"};
                    String[] values = {'\'' + teamID.getText() + '\'', databaseManager.getDepartmentByName((String) depName.getSelectedItem())};
                    success = databaseManager.insert("TEAMS", columns, values);
                    insertWindow.resultMessage(success);
                }
            }
        });

        String[] labels = {"№ бригады", "Отдел"};
        JComponent[] components = {teamID, depName};
        insertWindow.addAllToGridPanel(labels, components, addButton, ipadX, ipadY);
    }

    void showTeamStructure() {
        JFrame teamWindow = new JFrame("Состав бригад");
        JPanel jPanel = new JPanel();
        teamWindow.setContentPane(jPanel);
        teamWindow.setVisible(true);
        teamWindow.setSize(500, 400);
        JScrollPane scrollPane = new JScrollPane();
        jPanel.add(scrollPane);
        JTextArea textArea = new JTextArea();
        jPanel.add(textArea);
        try {
            ResultSet rs = databaseManager.select("SELECT TEAM_ID, FIRST_NAME, LAST_NAME, DEP_NAME, CHIEF_ID from employees join teams using (team_id) join departments using(dep_id) order by team_id");
            int index = 1, prevIndex = 1;
            String teamMembers = "";
            while (rs.next()) {
                index = Integer.parseInt(rs.getString("TEAM_ID"));
                teamMembers += rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME") + ", ";
                if (prevIndex != index) {
                    textArea.append(teamMembers);
                    textArea.append("\n");
                    teamMembers = "";
                }
                prevIndex = index;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
