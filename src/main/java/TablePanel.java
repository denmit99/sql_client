import javax.swing.*;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;


public interface TablePanel {

    void addTopPanel();

    void addBottomPanel();

    void showAll();

    default JTable resultToTable(ResultSet rs, String[] header, String[] columnNames) {

        Vector<Vector<String>> dataVector = new Vector<Vector<String>>();
        Vector<String> headerVector = new Vector<String>();

        for (int i = 0; i < header.length; i++) {
            headerVector.add(header[i]);
        }

        try {
            while (rs.next()) {
                Vector<String> row = new Vector<String>();
                for (int i = 0; i < columnNames.length; i++) {
                    String s = rs.getString(columnNames[i]);
                    if (s != null) {
                        row.add(s.equals("Y") ? "+" : (s.equals("N") ? "" : s));
                    } else row.add(s);

                }
                dataVector.add(row);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        JTable table = new JTable(dataVector, headerVector);
        return table;
    }

    default String toDate(String date) {
        return "TO_DATE(\'" + date + "\', 'DD/MM/YY')";
    }

    default String toDateTime(String date) {
        return "TO_DATE(\'" + date + "\', 'DD.MM.YYYY HH24:MI:SS')";
    }

    default String toDateCurrentTime(String date) {
        return "TO_DATE(\'" + date + "\', 'YYYY-MM-DD HH24:MI:SS')";
    }

    default String quote(String s) {
        return "\'" + s + "\'";
    }

    default InsertWindow initInsertWindow(String name, String[] labels, JComponent[] components, JButton addButton) {
        int ipadX = 100, ipadY = 15;
        final InsertWindow insertWindow = new InsertWindow(name, 500, ipadY * (labels.length + 1) + 200);
        insertWindow.addAllToGridPanel(labels, components, addButton, ipadX, ipadY);
        return insertWindow;
    }

}
