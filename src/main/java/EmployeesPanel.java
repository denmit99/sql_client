import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class EmployeesPanel extends JPanel implements TablePanel {

    private DatabaseManager databaseManager;

    public EmployeesPanel(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        setLayout(new BorderLayout());
        setBackground(MainWindow.backgroundColor);
        addTopPanel();
        addBottomPanel();
        showAll();
    }

    @Override
    public void addTopPanel() {
        JPanel topPanel = new JPanel();
        topPanel.add(new Label("Работники"));
        JButton addButton = new JButton("Добавить работника");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showInsertWindow();
            }
        });
        topPanel.add(addButton);
        JButton medExamButton = new JButton("Медосмотр пилота");
        medExamButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showMedExamWindow();
            }
        });
        topPanel.add(medExamButton);
        add(topPanel, BorderLayout.NORTH);
    }

    @Override
    public void addBottomPanel() {
        JPanel bottomPanel = new JPanel();
        bottomPanel.add(new JLabel("Запросы"));
        add(bottomPanel, BorderLayout.SOUTH);
    }

    public void showAll() {
        String[] header = {"Имя", "Фамилия", "Возраст", "Пол", "Наличие детей", "Количество детей", "Дата приема на работу", "Зарплата", "Отдел"};
        String[] columnNames = {"first_name", "last_name", "age", "gender", "children", "children_num", "hire_date", "salary", "dep_name"};
        JScrollPane jScrollPane = new JScrollPane(resultToTable(databaseManager.select(
                "select first_name, last_name, age, gender, children, children_num, hire_date, salary, dep_name from employees join teams using(team_id) join departments using(dep_id)"),
                header, columnNames
        ));
        add(jScrollPane, BorderLayout.CENTER);
    }

    public ResultSet getTable() {
        return databaseManager.getAllFromTable("EMPLOYEES");
    }

    public void showInsertWindow() {
        final JTextField firstName = new JTextField();
        final JTextField lastName = new JTextField();
        final JTextField age = new JTextField();
        String[] itemsGender = {"M", "Ж"};
        final JComboBox gender = new JComboBox(itemsGender);
        final JCheckBox children = new JCheckBox();
        final JTextField childrenNum = new JTextField();
        final JTextField hireDate = new JTextField();
        final JTextField salary = new JTextField();
        String[] itemsTeam = databaseManager.getTeamIDs();
        final JComboBox team = new JComboBox(itemsTeam);
        JButton addButton = new JButton("Добавить");
        String[] labels = {"Имя", "Фамилия", "Пол", "Возраст", "Наличие детей", "Количество детей", "Дата приема на работу", "Зарплата", "Бригада"};
        JComponent[] components = {firstName, lastName, gender, age, children, childrenNum, hireDate, salary, team};
        InsertWindow insertWindow = initInsertWindow("Добавление работника", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String success;
                String[] columns = {"FIRST_NAME", "LAST_NAME", "GENDER", "AGE", "CHILDREN", "CHILDREN_NUM", "HIRE_DATE", "SALARY", "TEAM_ID"};
                String[] values = {'\'' + firstName.getText() + '\'', '\'' + lastName.getText() + '\'', '\'' +
                        (String) gender.getSelectedItem() + '\'', age.getText(), (children.isSelected() ? "\'Y\'" : "\'N\'"),
                        childrenNum.getText(), toDate(hireDate.getText()), salary.getText(), (String) team.getSelectedItem()};
                if ((!firstName.getText().equals("")) && (!lastName.getText().equals("")) && (!age.getText().equals(""))) {
                    success = databaseManager.insert("EMPLOYEES", columns, values);
                    insertWindow.resultMessage(success);
                }
            }
        });
    }

    public void showMedExamWindow() {
        final JComboBox pilot = new JComboBox(databaseManager.getPilots());
        final JTextField medExamDate = new JTextField();
        JButton addButton = new JButton("Обновить");
        String[] labels = {"Пилот", "Дата медосмотра"};
        JComponent[] components = {pilot, medExamDate};
        InsertWindow insertWindow = initInsertWindow("Медосмотр", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String success;
                if (!medExamDate.getText().equals("")) {
                    success = databaseManager.updateMedExamDate(databaseManager.getPilotByName((String) pilot.getSelectedItem()), toDate(medExamDate.getText()));
                    insertWindow.resultMessage(success);
                }
            }
        });
    }
}
