import javax.swing.*;
import java.awt.*;

public class InsertWindow extends JFrame{

    private JPanel jPanel;

    public InsertWindow(String windowName, int width, int height) {

        super(windowName);
        setVisible(true);
        setSize(width, height);
        setLocationRelativeTo(null);
        jPanel = new JPanel();
        setContentPane(jPanel);
        jPanel.setLayout(new GridBagLayout());
    }

    public  void addAllToGridPanel(String[] labels, JComponent[] components, JButton addButton, int ipadx, int ipady){
        for (int i = 0; i < labels.length; i++) {
            addToGridPanel(new JLabel(labels[i]), 0, i, ipadx, ipady, 1, 1);
            addToGridPanel(components[i], 1, i, ipadx, ipady, 1, 1);
        }
        addToGridPanel(addButton, 0, labels.length, ipadx, ipady, 2, 1);
    }

    public void addToGridPanel(JComponent component, int gridx, int gridy, int ipadx, int ipady, int gridwidth, int gridheight) {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = gridx;
        constraints.gridy = gridy;
        constraints.ipadx = ipadx;
        constraints.ipady = ipady;
        constraints.gridwidth = gridwidth;
        constraints.gridheight = gridheight;
        jPanel.add(component, constraints);
    }

    void resultMessage(String result) {
        if (result.equals("")) {
            JOptionPane.showMessageDialog(this, "Данные успешно добавлены!");
        } else {
            JOptionPane.showMessageDialog(this, "Введены неверные данные\n" + result, "Ошибка", JOptionPane.ERROR_MESSAGE);
        }
    }
}
