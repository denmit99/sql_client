import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

public class PlanesPanel extends JPanel implements TablePanel {

    private DatabaseManager databaseManager;
    private JScrollPane contentPanel = null;

    public PlanesPanel(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        setLayout(new BorderLayout());
        setBackground(MainWindow.backgroundColor);
        addTopPanel();
        addBottomPanel();
        showAll();
    }

    @Override
    public void addTopPanel() {
        JPanel topPanel = new JPanel();
        topPanel.add(new Label("Таблицы"));
        JButton allPlanesButton = new JButton("Все самолеты");
        allPlanesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showAll();
            }
        });
        topPanel.add(allPlanesButton);
        JButton myPlanesButton = new JButton("Мои самолеты");
        myPlanesButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showAllMyPlanes();
            }
        });
        topPanel.add(myPlanesButton);
        topPanel.add(new Label("Действия"));
        JButton addButton = new JButton("Добавить самолёт");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showInsertWindow();
            }
        });
        topPanel.add(addButton);
        JButton myPlaneButton = new JButton("Приписать к аэропорту");
        myPlaneButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showMyPlaneWindow();
            }
        });
        topPanel.add(myPlaneButton);
        JButton sendToRepairButton = new JButton("Отправить в ремонт");
        sendToRepairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendToRepairWindow();
            }
        });
        topPanel.add(sendToRepairButton);
        JButton inspectionButton = new JButton("Техосмотр");
        inspectionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                inspectionWindow();
            }
        });
        topPanel.add(inspectionButton);
        add(topPanel, BorderLayout.NORTH);
    }

    @Override
    public void addBottomPanel() {
        JPanel bottomPanel = new JPanel();
        bottomPanel.add(new JLabel("Запросы"));
        JButton repNumButton = new JButton("Кол-во ремонтов");
        repNumButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                repNumWindow();
            }
        });
        bottomPanel.add(repNumButton);
        add(bottomPanel, BorderLayout.SOUTH);
    }

    @Override
    public void showAll() {
        if (contentPanel != null) {
            this.remove(contentPanel);
        }
        String[] header = {"№ самолета", "Тип", "Кол-во мест"};
        String[] columnNames = {"plane_id", "type_name", "num_of_seats"};
        contentPanel = new JScrollPane(resultToTable(databaseManager.select(
                "select plane_id, type_name, num_of_seats from planes join planetypes using(type_id) order by plane_id"),
                header, columnNames));
        add(contentPanel, BorderLayout.CENTER);
        contentPanel.setVisible(true);
        revalidate();
    }

    public void showAllMyPlanes() {
        if (contentPanel != null) {
            remove(contentPanel);
        }
        String[] header = {"№ самолета", "Тип", "Кол-во мест", "Пилоты", "Техники", "Сервис", "Дата постройки", "Последний техосмотр"};
        String[] columnNames = {"MY_PLANE_ID", "TYPE_NAME", "NUM_OF_SEATS", "PILOTS_TEAM", "TECH_TEAM", "SERVICE_TEAM", "CONSTRUCTION_DATE", "INSPECTION_DATE"};
        contentPanel = new JScrollPane(resultToTable(databaseManager.select(
                "select MY_PLANE_ID, TYPE_NAME, NUM_OF_SEATS, PILOTS_TEAM, TECH_TEAM, SERVICE_TEAM, CONSTRUCTION_DATE," +
                        " INSPECTION_DATE from myplanes join planes using(plane_id) join planetypes using (type_id) order by MY_PLANE_ID"),
                header, columnNames));
        add(contentPanel, BorderLayout.CENTER);
        contentPanel.setVisible(true);
        revalidate();
    }

    public void showInsertWindow() {
        final JTextField planeID = new JTextField();
        final JComboBox planeType = new JComboBox(databaseManager.getPlaneTypes());
        JButton addButton = new JButton("Добавить");
        String[] labels = {"№ самолёта", "Тип"};
        JComponent[] components = {planeID, planeType};
        InsertWindow insertWindow = initInsertWindow("Добавление самолёта", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String success;
                if (!planeID.getText().equals("")) {
                    String[] columns = {"PLANE_ID", "TYPE_ID"};
                    String[] values = {quote(planeID.getText()), databaseManager.getPlaneTypeByName((String) planeType.getSelectedItem())};
                    success = databaseManager.insert("PLANES", columns, values);
                    insertWindow.resultMessage(success);
                }
            }
        });
    }

    public void showMyPlaneWindow() {

        final JTextField planeID = new JTextField();
        final JComboBox pilots = new JComboBox(databaseManager.getTeams("Пилоты"));
        final JComboBox tech = new JComboBox(databaseManager.getTeams("Техники"));
        final JComboBox service = new JComboBox(databaseManager.getTeams("Обслуживающий персонал"));
        final JTextField constructionDate = new JTextField();
        JButton addButton = new JButton("Добавить");
        String[] labels = {"№ самолёта", "Бригада пилотов", "Бригада техников", "Бригада обслуж. персонала", "Дата постройки"};
        JComponent[] components = {planeID, pilots, tech, service, constructionDate};
        InsertWindow insertWindow = initInsertWindow("Приписать самолет", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String success;
                if ((!planeID.getText().equals("") && !constructionDate.getText().equals(""))) {
                    String[] columns = {"PLANE_ID", "PILOTS_TEAM", "TECH_TEAM", "SERVICE_TEAM", "CONSTRUCTION_DATE"};
                    String[] values = {quote(planeID.getText()), (String) pilots.getSelectedItem(),
                            (String) tech.getSelectedItem(), (String) service.getSelectedItem(), toDate(constructionDate.getText())};
                    success = databaseManager.insert("MYPLANES", columns, values);
                    insertWindow.resultMessage(success);
                }
            }
        });
    }

    public void sendToRepairWindow() {
        final JTextField planeID = new JTextField();
        final JTextField sendDate = new JTextField();
        JButton addButton = new JButton("Отправить");
        String[] labels = {"ID самолета", "Дата отправки в ремонт"};
        JComponent[] components = {planeID, sendDate};
        InsertWindow insertWindow = initInsertWindow("Отправить в ремонт", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String success;
                String[] columns = {"MY_PLANE_ID", "SENDING_TIME"};
                String[] values = {planeID.getText(), toDate(sendDate.getText())};
                if ((!planeID.getText().equals("")) && (!sendDate.getText().equals(""))) {
                    success = databaseManager.insert("PLANEREPAIRS", columns, values);
                    insertWindow.resultMessage(success);
                }
            }
        });
    }

    public void inspectionWindow() {
        final JTextField planeID = new JTextField();
        final JTextField inspectionDate = new JTextField();
        JButton addButton = new JButton("Обновить");
        String[] labels = {"ID самолета", "Дата техосмотра"};
        JComponent[] components = {planeID, inspectionDate};
        InsertWindow insertWindow = initInsertWindow("Техосмотр", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String success;
                if ((!planeID.getText().equals("")) && (!inspectionDate.getText().equals(""))) {
                    success = databaseManager.updateInspectionDate(planeID.getText(), toDate(inspectionDate.getText()));
                    insertWindow.resultMessage(success);
                }
            }
        });
    }

    public void repNumWindow() {
        JLabel textFrom = new JLabel("Количество ремонтов от");
        final JTextField valueFrom = new JTextField("0");
        JLabel textTo = new JLabel("до");
        final JTextField valueTo = new JTextField();
        JButton button = new JButton("OK");
        JPanel resultPanel = new JPanel();
        JScrollPane result = new JScrollPane(resultPanel);
        int ipadx = 50, ipady = 15;
        InsertWindow window = new InsertWindow("Кол-во ремонтов", 500, 300);

        window.addToGridPanel(textFrom, 0, 0, 25, ipady, 1, 1);
        window.addToGridPanel(valueFrom, 1, 0, ipadx, ipady, 1, 1);
        window.addToGridPanel(textTo, 2, 0, 25, ipady, 1, 1);
        window.addToGridPanel(valueTo, 3, 0, ipadx, ipady, 1, 1);
        window.addToGridPanel(button, 0, 1, ipadx, ipady, 4, 1);
        window.addToGridPanel(result, 0, 2, ipadx * 4, 100, 4, 1);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (valueFrom.getText().length() > 0 || valueTo.getText().length() > 0) {
                    String from = valueFrom.getText();
                    String to = valueTo.getText();
                    ResultSet resultSet = databaseManager.getRepairNumber(from, to);
                    String[] header = {"№ самолета", "Кол-во ремонтов"};
                    String[] columnNames = {"MY_PLANE_ID", "NUMBER_OF_REPAIRS"};
                    JTable table = resultToTable(resultSet, header, columnNames);
                    resultPanel.removeAll();
                    resultPanel.add(table);
                    window.revalidate();
                }
            }
        });
    }
}
