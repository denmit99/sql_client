import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import java.util.Vector;

public class ClientsPanel extends JPanel implements TablePanel {

    private DatabaseManager databaseManager;

    public ClientsPanel(DatabaseManager databaseManager) {
        this.databaseManager = databaseManager;
        setLayout(new BorderLayout());
        setBackground(MainWindow.backgroundColor);
        addTopPanel();
        addBottomPanel();
        showAll();
    }

    @Override
    public void addTopPanel() {
        JPanel topPanel = new JPanel();
        topPanel.add(new Label("Клиенты"));
        JButton addButton = new JButton("Добавить клиента");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showInsertWindow();
            }
        });
        topPanel.add(addButton);
        add(topPanel, BorderLayout.NORTH);
    }

    @Override
    public void addBottomPanel() {
        JPanel bottomPanel = new JPanel();
        bottomPanel.add(new JLabel("Запросы"));
        add(bottomPanel, BorderLayout.SOUTH);
    }

    public void showAll() {
        String[] header = {"Имя", "Фамилия", "Пол", "Возраст"};
        String[] columnNames = {"FIRST_NAME", "LAST_NAME", "GENDER", "AGE"};
        JScrollPane jScrollPane = new JScrollPane(resultToTable(getTable(), header, columnNames));
        add(jScrollPane, BorderLayout.CENTER);
    }

    public ResultSet getTable() {
        return databaseManager.getAllFromTable("PASSENGERS");
    }

    public void showInsertWindow() {
        int ipadX = 100, ipadY = 15, iPadTextX = 300;
        final InsertWindow insertWindow = new InsertWindow("Добавление клиента", 450, 250);

        final JTextField firstName = new JTextField();
        final JTextField lastName = new JTextField();
        String[] items = {"М", "Ж"};
        final JComboBox gender = new JComboBox(items);
        final JTextField age = new JTextField();
        JButton addButton = new JButton("Добавить");
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String success;
                String[] columns = {"FIRST_NAME", "LAST_NAME", "GENDER", "AGE"};
                String[] values = {'\'' + firstName.getText() + '\'', '\'' + lastName.getText() + '\'', '\'' +
                        (String) gender.getSelectedItem() + '\'', age.getText()};

                if ((!firstName.getText().equals("")) && (!lastName.getText().equals("")) && (!age.getText().equals(""))) {
                    success = databaseManager.insert("CLIENTS", columns, values);
                    insertWindow.resultMessage(success);
                }
            }
        });

        String[] labels = {"Имя", "Фамилия", "Пол", "Возраст"};
        JComponent[] components = {firstName, lastName, gender, age};
        insertWindow.addAllToGridPanel(labels, components, addButton, ipadX, ipadY);
    }

}
