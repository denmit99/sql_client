import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;

public class TicketsPanel extends JPanel implements TablePanel {

    private DatabaseManager databaseManager;
    private JScrollPane contentPanel = null;

    public TicketsPanel(DatabaseManager databaseManager) {

        this.databaseManager = databaseManager;
        setLayout(new BorderLayout());
        setBackground(MainWindow.backgroundColor);
        addTopPanel();
        addBottomPanel();
        showAll();
    }

    @Override
    public void addTopPanel() {
        JPanel topPanel = new JPanel();
        topPanel.add(new Label("Таблицы"));
        JButton allTicketsButton = new JButton("Всe билеты");
        allTicketsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showAll();
            }
        });
        topPanel.add(allTicketsButton);
        JButton bookedTicketsButton = new JButton("Забронированные");
        bookedTicketsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showAllBooked();
            }
        });
        topPanel.add(bookedTicketsButton);
        topPanel.add(new Label("Действия"));
        JButton sellButton = new JButton("Продажа");
        sellButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showSellWindow();
            }
        });
        JButton bookButton = new JButton("Бронь");
        bookButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showBookWindow();
            }
        });
        topPanel.add(bookButton);
        topPanel.add(sellButton);
        JButton buyButton = new JButton("Выкуп брони");
        buyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showBuyWindow();
            }
        });
        topPanel.add(buyButton);
        JButton returnButton = new JButton("Возврат");
        returnButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showReturnWindow();
            }
        });
        topPanel.add(returnButton);
        add(topPanel, BorderLayout.NORTH);
    }

    @Override
    public void addBottomPanel() {
        JPanel bottomPanel = new JPanel();
        bottomPanel.add(new JLabel("Запросы"));
        add(bottomPanel, BorderLayout.SOUTH);
    }

    public void showAll() {
        if (contentPanel != null) {
            this.remove(contentPanel);
        }
        String[] header = {"№ билета", "Имя", "Фамилия", "Откуда", "Пересадка", "Куда", "Время брони", "Время покупки", "Время возврата", "Дата рейса", "Багаж"};
        String[] columnNames = {"TICKET_ID", "FIRST_NAME", "LAST_NAME", "START_POINT", "TRANSFER_POINT", "END_POINT", "BOOKING_TIME", "PURCHASE_TIME", "RETURN_TIME", "FLIGHT_DATE", "BAGGAGE"};
        contentPanel = new JScrollPane(resultToTable(databaseManager.select(
                "select ticket_id, FIRST_NAME, LAST_NAME, START_POINT,TRANSFER_POINT, END_POINT, BOOKING_TIME, " +
                        "PURCHASE_TIME, RETURN_TIME, flight_date, baggage  from tickets join passengers using(pass_id)" +
                        " join plannedflights using(flight_id) join timetable using(reg_flight_id) join routes using(route_id) order by purchase_time"),
                header, columnNames
        ));
        add(contentPanel, BorderLayout.CENTER);
        contentPanel.setVisible(true);
        revalidate();
    }


    public void showAllBooked() {
        if (contentPanel != null) {
            this.remove(contentPanel);
        }
        String[] header = {"№ билета", "Имя", "Фамилия", "Откуда", "Пересадка", "Куда", "Время брони", "Время покупки", "Время возврата", "Дата рейса", "Багаж"};
        String[] columnNames = {"TICKET_ID", "FIRST_NAME", "LAST_NAME", "START_POINT", "TRANSFER_POINT", "END_POINT", "BOOKING_TIME", "PURCHASE_TIME", "RETURN_TIME", "FLIGHT_DATE", "BAGGAGE"};
        contentPanel = new JScrollPane(resultToTable(databaseManager.select(
                "select ticket_id, FIRST_NAME, LAST_NAME, START_POINT,TRANSFER_POINT, END_POINT, BOOKING_TIME," +
                        " PURCHASE_TIME, RETURN_TIME, flight_date, baggage  from tickets join passengers" +
                        " using(pass_id) join plannedflights using(flight_id) join timetable using(reg_flight_id) " +
                        "join routes using(route_id) where booking_time is not null and purchase_time is null"),
                header, columnNames
        ));
        add(contentPanel, BorderLayout.CENTER);
        contentPanel.setVisible(true);
        revalidate();
    }

    public void showBookWindow() {
        final JComboBox flightID = new JComboBox(databaseManager.getFlights());
        final JComboBox passName = new JComboBox(databaseManager.getClients());
        JButton addButton = new JButton("Добавить");
        String[] labels = {"№ рейса", "Пассажир"};
        JComponent[] components = {flightID, passName};
        InsertWindow insertWindow = initInsertWindow("Бронирование билета", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] columns = {"FLIGHT_ID", "PASS_ID", "BOOKING_TIME"};
                String[] values = {(String) flightID.getSelectedItem(),
                        databaseManager.getPassengerByName((String) passName.getSelectedItem()),
                        toDateCurrentTime(databaseManager.getTime())};
                String success = databaseManager.insert("TICKETS", columns, values);
                insertWindow.resultMessage(success);
            }
        });
    }

    public void showSellWindow() {
        final JComboBox flightID = new JComboBox(databaseManager.getFlights());
        final JComboBox passName = new JComboBox(databaseManager.getClients());
        JButton addButton = new JButton("Добавить");
        String[] labels = {"№ рейса", "Пассажир"};
        JComponent[] components = {flightID, passName};
        InsertWindow insertWindow = initInsertWindow("Продажа билета", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] columns = {"FLIGHT_ID", "PASS_ID", "PURCHASE_TIME"};
                String[] values = {(String) flightID.getSelectedItem(),
                        databaseManager.getPassengerByName((String) passName.getSelectedItem()),
                        toDateCurrentTime(databaseManager.getTime())};
                String success = databaseManager.insert("TICKETS", columns, values);
                insertWindow.resultMessage(success);
            }
        });
    }

    public void showBuyWindow() {

        final JComboBox ticketID = new JComboBox(databaseManager.getBookedTickets());
        JButton addButton = new JButton("Выкупить");
        String[] labels = {"№ рейса"};
        JComponent[] components = {ticketID};
        InsertWindow insertWindow = initInsertWindow("Выкупка забронированного билета", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] columns = {"PURCHASE_TIME"};
                String[] values = {toDateCurrentTime(databaseManager.getTime())};
                String success = databaseManager.update("TICKETS", columns, values, "TICKET_ID", (String) ticketID.getSelectedItem());
                insertWindow.resultMessage(success);
            }
        });
    }

    public void showReturnWindow() {

        final JComboBox ticketID = new JComboBox(databaseManager.getTickets());
        JButton addButton = new JButton("Вернуть");
        String[] labels = {"№ рейса"};
        JComponent[] components = {ticketID};
        InsertWindow insertWindow = initInsertWindow("Возврат билета", labels, components, addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String[] columns = {"RETURN_TIME"};
                String[] values = {toDateCurrentTime(databaseManager.getTime())};
                String success = databaseManager.update("TICKETS", columns, values, "TICKET_ID", (String) ticketID.getSelectedItem());
                insertWindow.resultMessage(success);
            }
        });
    }
}
