import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class MainWindow extends JFrame {

    private DatabaseManager databaseManager;
    private JButton timetableButton = new JButton("Расписание");
    private JButton planesButton = new JButton("Самолеты");
    private JButton ticketsButton = new JButton("Билеты");
    private JButton employeesButton = new JButton("Работники");
    private JButton teamsButton = new JButton("Бригады");
    private JButton clientsButton = new JButton("Клиенты");
    private JPanel contentPanel;
    private JPanel mainPanel = new JPanel();
    private JPanel menuPanel = new JPanel();
    public static final Color backgroundColor = new Color(182, 206, 255);

    public MainWindow(int sizeWidth, int sizeHeight) throws SQLException {
        super("Информационная система аэропорта");

        String username = "username";
        String password = "password";
        String url = "jdbc:oracle:thin:@84.237.50.81:1521:";

        setSize(sizeWidth, sizeHeight);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage("src/main/resources/logo.png"));
        databaseManager = new DatabaseManager(url, username, password);
        contentPanel = new TimetablePanel(databaseManager);


        setContentPane(mainPanel);
        mainPanel.setLayout(new BorderLayout());
        mainPanel.add(menuPanel, BorderLayout.NORTH);
        menuPanel.setBackground(new Color(39, 102, 246));
        mainPanel.add(contentPanel, BorderLayout.CENTER);
        menuPanel.add(timetableButton);
        menuPanel.add(planesButton);
        menuPanel.add(ticketsButton);
        menuPanel.add(employeesButton);
        menuPanel.add(teamsButton);
        menuPanel.add(clientsButton);
        linkButtons();
    }

    public void linkButtons() {
        timetableButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mainPanel.remove(contentPanel);
                contentPanel = new TimetablePanel(databaseManager);
                mainPanel.add(contentPanel);
                setVisible(true);
            }
        });
        ticketsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mainPanel.remove(contentPanel);
                contentPanel = new TicketsPanel(databaseManager);
                mainPanel.add(contentPanel);
                setVisible(true);
            }
        });
        employeesButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mainPanel.remove(contentPanel);
                contentPanel = new EmployeesPanel(databaseManager);
                mainPanel.add(contentPanel);
                setVisible(true);
            }
        });
        clientsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mainPanel.remove(contentPanel);
                contentPanel = new ClientsPanel(databaseManager);
                mainPanel.add(contentPanel);
                setVisible(true);
            }
        });
        teamsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mainPanel.remove(contentPanel);
                contentPanel = new TeamsPanel(databaseManager);
                mainPanel.add(contentPanel);
                setVisible(true);
            }
        });
        planesButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mainPanel.remove(contentPanel);
                contentPanel = new PlanesPanel(databaseManager);
                mainPanel.add(contentPanel);
                setVisible(true);
            }
        });
    }
}
