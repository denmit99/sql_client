import java.io.Closeable;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class DatabaseManager implements Closeable {

    private final String url;
    private final String userName;
    private final String password;
    private static Connection connection;
    private static Statement statement;

    public DatabaseManager(String url, String userName, String password) {
        this.url = url;
        this.userName = userName;
        this.password = password;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection(url, userName, password);
            statement = connection.createStatement();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public String getTime() {
        try {
            ResultSet rs = statement.executeQuery("select LOCALTIMESTAMP from dual");
            rs.next();
            String time = rs.getString("LOCALTIMESTAMP");
            String time2 = String.copyValueOf(time.toCharArray(), 0, time.length() - 4);
            return time2;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String[] getTeamIDs() {
        ArrayList<String> ids = new ArrayList<>();
        ResultSet rs;
        try {
            rs = statement.executeQuery("SELECT DISTINCT TEAM_ID FROM TEAMS ORDER BY TEAM_ID");
            while (rs.next()) {
                ids.add(rs.getString("TEAM_ID"));
            }
            String[] teamIDs = new String[ids.size()];
            ids.toArray(teamIDs);
            return teamIDs;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String[] getFlights() {
        ArrayList<String> ids = new ArrayList<>();
        ResultSet rs;
        try {
            rs = statement.executeQuery("SELECT DISTINCT FLIGHT_ID FROM PLANNEDFLIGHTS ORDER BY FLIGHT_ID ");
            while (rs.next()) {
                ids.add(rs.getString("FLIGHT_ID"));
            }
            String[] flightIDs = new String[ids.size()];
            ids.toArray(flightIDs);
            return flightIDs;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String[] getRegularFlights() {
        ArrayList<String> ids = new ArrayList<>();
        ResultSet rs;
        try {
            rs = statement.executeQuery("SELECT DISTINCT REG_FLIGHT_ID FROM TIMETABLE ORDER BY REG_FLIGHT_ID");
            while (rs.next()) {
                ids.add(rs.getString("REG_FLIGHT_ID"));
            }
            String[] flightIDs = new String[ids.size()];
            ids.toArray(flightIDs);
            return flightIDs;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String[] getDepartments() {
        ArrayList<String> depList = new ArrayList<>();
        ResultSet rs;
        try {
            rs = statement.executeQuery("SELECT DISTINCT DEP_NAME FROM DEPARTMENTS");
            while (rs.next()) {
                depList.add(rs.getString("DEP_NAME"));
            }
            String[] deps = new String[depList.size()];
            depList.toArray(deps);
            return deps;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String[] getPlanes() {
        ArrayList<String> typeList = new ArrayList<>();
        ResultSet rs;
        try {
            rs = statement.executeQuery("SELECT DISTINCT PLANE_ID FROM PLANES");
            while (rs.next()) {
                typeList.add(rs.getString("PLANE_ID"));
            }
            String[] types = new String[typeList.size()];
            typeList.toArray(types);
            return types;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String[] getPlaneTypes() {
        ArrayList<String> typeList = new ArrayList<>();
        ResultSet rs;
        try {
            rs = statement.executeQuery("SELECT DISTINCT TYPE_NAME FROM PLANETYPES");
            while (rs.next()) {
                typeList.add(rs.getString("TYPE_NAME"));
            }
            String[] types = new String[typeList.size()];
            typeList.toArray(types);
            return types;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String[] getPilots() {
        ArrayList<String> clientsList = new ArrayList<>();
        ResultSet rs;
        try {
            rs = statement.executeQuery("SELECT DISTINCT FIRST_NAME, LAST_NAME FROM PILOTS JOIN EMPLOYEES USING(EMPLOYEE_ID) ORDER BY FIRST_NAME");
            while (rs.next()) {
                clientsList.add(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
            }
            String[] clients = new String[clientsList.size()];
            clientsList.toArray(clients);
            return clients;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String[] getClients() {
        ArrayList<String> clientsList = new ArrayList<>();
        ResultSet rs;
        try {
            rs = statement.executeQuery("SELECT DISTINCT FIRST_NAME, LAST_NAME FROM PASSENGERS ORDER BY FIRST_NAME");
            while (rs.next()) {
                clientsList.add(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"));
            }
            String[] clients = new String[clientsList.size()];
            clientsList.toArray(clients);
            return clients;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String[] getTeams(String team) {
        ArrayList<String> teamList = new ArrayList<>();
        ResultSet rs;
        try {
            rs = statement.executeQuery("SELECT DISTINCT TEAM_ID FROM TEAMS JOIN DEPARTMENTS USING(dep_id) JOIN EMPLOYEES USING(team_id) WHERE dep_name=\'" + team + "\' AND NOT (employee_id=chief_id) ORDER BY TEAM_ID");
            while (rs.next()) {
                teamList.add(rs.getString("TEAM_ID"));
            }
            String[] types = new String[teamList.size()];
            teamList.toArray(types);
            return types;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String[] getDelayReasons() {
        ArrayList<String> teamList = new ArrayList<>();
        ResultSet rs;
        try {
            rs = statement.executeQuery("SELECT REASON_NAME FROM DELAYREASONS");
            while (rs.next()) {
                teamList.add(rs.getString("REASON_NAME"));
            }
            String[] types = new String[teamList.size()];
            teamList.toArray(types);
            return types;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String[] getTickets() {
        ArrayList<String> ids = new ArrayList<>();
        ResultSet rs;
        try {
            rs = statement.executeQuery("SELECT DISTINCT TICKET_ID FROM TICKETS ORDER BY TICKET_ID");
            while (rs.next()) {
                ids.add(rs.getString("TICKET_ID"));
            }
            String[] ticketIDs = new String[ids.size()];
            ids.toArray(ticketIDs);
            return ticketIDs;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String[] getBookedTickets() {
        ArrayList<String> ids = new ArrayList<>();
        ResultSet rs;
        try {
            rs = statement.executeQuery("SELECT DISTINCT TICKET_ID FROM TICKETS WHERE BOOKING_TIME IS NOT NULL ORDER BY TICKET_ID");
            while (rs.next()) {
                ids.add(rs.getString("TICKET_ID"));
            }
            String[] ticketIDs = new String[ids.size()];
            ids.toArray(ticketIDs);
            return ticketIDs;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String getDepartmentByName(String depName) {

        try {
            ResultSet rs = statement.executeQuery("SELECT DEP_ID FROM DEPARTMENTS WHERE DEP_NAME = \'" + depName + "\'");
            rs.next();
            return rs.getString("DEP_ID");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String getPlaneTypeByName(String typeName) {

        try {
            ResultSet rs = statement.executeQuery("SELECT TYPE_ID FROM PLANETYPES WHERE TYPE_NAME = \'" + typeName + "\'");
            rs.next();
            return rs.getString("TYPE_ID");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String getPassengerByName(String name) {

        try {
            ResultSet rs = statement.executeQuery("SELECT PASS_ID, FIRST_NAME, LAST_NAME FROM PASSENGERS");
            while (rs.next()) {
                if (name.equals(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"))) {
                    return rs.getString("PASS_ID");
                }
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String getPilotByName(String name) {
        try {
            ResultSet rs = statement.executeQuery("SELECT DISTINCT PILOT_ID, FIRST_NAME, LAST_NAME  FROM PILOTS JOIN EMPLOYEES USING(employee_id)");
            while (rs.next()) {
                if (name.equals(rs.getString("FIRST_NAME") + " " + rs.getString("LAST_NAME"))) {
                    return rs.getString("PILOT_ID");
                }
            }
            return null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String getDelayReasonByName(String typeName) {
        try {
            ResultSet rs = statement.executeQuery("SELECT REASON_ID FROM DELAYREASONS WHERE REASON_NAME = \'" + typeName + "\'");
            rs.next();
            return rs.getString("REASON_ID");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String getRouteIdByFlightId(String s) {
        try {
            int flightId = Integer.parseInt(s);
            CallableStatement callableStatement = connection.prepareCall("{? = call ROUTEID_BY_FLIGHT_ID(?)}");
            callableStatement.registerOutParameter(1, Types.INTEGER);
            callableStatement.setInt(2, flightId);
            callableStatement.execute();
            return String.valueOf(callableStatement.getInt(1));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String getPlaneIdByFlightId(String s) {
        try {
            int flightId = Integer.parseInt(s);
            CallableStatement callableStatement = connection.prepareCall("{? = call PLANEID_BY_FLIGHT_ID(?)}");
            callableStatement.registerOutParameter(1, Types.INTEGER);
            callableStatement.setInt(2, flightId);
            callableStatement.execute();
            return String.valueOf(callableStatement.getInt(1));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String getFlightTypeIdByFlightId(String s) {
        try {
            int flightId = Integer.parseInt(s);
            CallableStatement callableStatement = connection.prepareCall("{? = call FLIGHTTYPEID_BY_FLIGHT_ID(?)}");
            callableStatement.registerOutParameter(1, Types.INTEGER);
            callableStatement.setInt(2, flightId);
            callableStatement.execute();
            return String.valueOf(callableStatement.getInt(1));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String getDurationByFlightId(String s) {
        try {
            int flightId = Integer.parseInt(s);
            CallableStatement callableStatement = connection.prepareCall("{? = call DURATION_BY_FLIGHT_ID(?)}");
            callableStatement.registerOutParameter(1, Types.INTEGER);
            callableStatement.setInt(2, flightId);
            callableStatement.execute();
            return String.valueOf(callableStatement.getInt(1));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public String getDateByFlightId(String s) {
        try {
            int flightId = Integer.parseInt(s);
            CallableStatement callableStatement = connection.prepareCall("{? = call DATE_BY_FLIGHT_ID(?)}");
            callableStatement.registerOutParameter(1, Types.DATE);
            callableStatement.setInt(2, flightId);
            callableStatement.execute();
            return String.valueOf(callableStatement.getInt(1));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return null;
        }
    }

    public ResultSet getAllFromTable(String tablename) {
        ResultSet rs = null;
        try {
            rs = statement.executeQuery("SELECT * FROM " + tablename);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return rs;
    }

    public ResultSet getRepairNumber(String from, String to) {
        String where = "";
        if (!from.equals("") && to.equals("")) {
            where = "where number_of_repairs >= " + from;
        } else if (from.equals("") && !to.equals("")) {
            where = "where number_of_repairs <= " + to;
        } else if (!from.equals("") && !to.equals("")) {
            where = "where number_of_repairs between " + from + " and " + to;
        }
        String select = "select distinct * from (select distinct my_plane_id, count(my_plane_id) " +
                "over (partition by my_plane_id) as number_of_repairs from myplanes join planerepairs" +
                " using (my_plane_id) where repair_time is not null) " + where + "  order by my_plane_id";
        return select(select);
    }

    public ResultSet select(String select) {
        ResultSet rs = null;
        try {
            rs = statement.executeQuery(select);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return rs;
    }

    public String insert(String table, String[] columns, String[] values) {
        String insert = "INSERT INTO " + table + "(";
        for (int i = 0; i < columns.length; i++) {
            insert += columns[i];
            if (i != columns.length - 1) {
                insert += ", ";
            }
        }
        insert += ") VALUES (";
        for (int i = 0; i < values.length; i++) {
            insert += values[i];
            if (i != values.length - 1) {
                insert += ", ";
            }
        }
        insert += ")";

        try {
            statement.executeUpdate(insert);
            return "";
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return throwables.getMessage();
        }
    }

    public String update(String table, String[] columns, String[] values, String condCol, String condVal) {
        String update = "UPDATE " + table + " SET ";
        for (int i = 0; i < columns.length; i++) {
            update += columns[i] + "=" + values[i];
            if (i != columns.length - 1) {
                update += ", ";
            }
        }
        update += " WHERE " + condCol + "=" + condVal;
        try {
            statement.executeUpdate(update);
            return "";
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return throwables.getMessage();
        }
    }

    public String updateMedExamDate(String id, String date) {
        String[] columns = {"MEDICAL_EXAM_DATE"};
        String[] values = {date};
        return update("PILOTS", columns, values, "PILOT_ID", id);
    }


    public String updateInspectionDate(String id, String date) {
        String[] columns = {"INSPECTION_DATE"};
        String[] values = {date};
        return update("MYPLANES", columns, values, "MY_PLANE_ID", id);
    }

    @Override
    public void close() {
        try {
            statement.close();
            connection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
