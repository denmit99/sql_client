import javax.swing.*;
import java.awt.*;
import java.sql.*;
import java.util.Locale;
import java.util.TimeZone;

public class Main {


    public static void main(String args[]) throws SQLException {

        TimeZone timeZone = TimeZone.getTimeZone("GMT+7");
        TimeZone.setDefault(timeZone);
        Locale.setDefault(Locale.ENGLISH);

        MainWindow app = new MainWindow(1000, 800);
        app.setVisible(true);

        System.out.println("END");
    }
}

